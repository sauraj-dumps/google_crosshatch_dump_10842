## crosshatch-user 11 RQ3A.211001.001 7641976 release-keys
- Manufacturer: google
- Platform: sdm845
- Codename: crosshatch
- Brand: google
- Flavor: crosshatch-user
- Release Version: 11
- Id: RQ3A.211001.001
- Incremental: 7641976
- Tags: release-keys
- CPU Abilist: arm64-v8a,armeabi-v7a,armeabi
- A/B Device: true
- Locale: en-US
- Screen Density: undefined
- Fingerprint: google/crosshatch/crosshatch:11/RQ3A.211001.001/7641976:user/release-keys
- OTA version: 
- Branch: crosshatch-user-11-RQ3A.211001.001-7641976-release-keys
- Repo: google_crosshatch_dump_10842


>Dumped by [Phoenix Firmware Dumper](https://github.com/DroidDumps/phoenix_firmware_dumper)
